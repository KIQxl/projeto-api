const express = require('express');
const bodyParser = require('express');
const app = express();
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
const port = 8000;

// habilitar servidor
app.listen(port,()=>{
    console.log("Projeto executando na porta: " + port);
});

app.get('/aluno', (req, res)=>{
    res.send("{mensage: Aluno encontrado}");
});

// recurso de resquest.query
app.get('/aluno/filtros', (req, res)=>{
    let source = req.query;
    let ret = "dados solicitados " + source.nome + " " + source.sobrenome;
    res.send("{message: " + ret + "}");
});

//recurso de resquest.param

app.get('/aluno/filtros/pesquisa/:valor' , (req, res)=>{
    console.log("entrou")
    let dado = req.params.valor;
    let ret = "dados solicitados " + dado;
    res.send("{message: " + ret + "}");
});

//recurso de resquest.body

app.post('/aluno',(req, res)=>{
    let dados = req.body;
    let ret = "{dados enviados: nome" + dados.nome;
    ret+=" sobrenome: " + dados.idade;
    ret+=" idade: " + dados.idade;
    res.send("{message: " +ret+ "}");
});